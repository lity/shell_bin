#!/bin/bash

# 解析出 commit id 
extract_cid() {
    echo "$1" | awk '{print $2}'
}

# 设置固定的文件名称
todo_file="git_rebase_todo.txt"
base_msgs_file="git_base_commit_messages.txt"

# 如果文件已存在，先删除它们
rm -f "$todo_file" "$base_msgs_file"

# 获取基础分支或commit中的所有commit messages并写入文件
base=$1
cat $2 > $todo_file
git log --format=%s $base > "$base_msgs_file"

# 从rebase -i 交互中获取有的commit
branch_only_commits=$(cat $todo_file)
echo -n "" > $todo_file


# 遍历当前分支独有的commits，并过滤掉message重复的
echo "$branch_only_commits" | while read -r line; do
	# 空白行和注释行, 原样输出
    if [[ "$line" == '#'* ]]; then
        echo "$line" >> "$todo_file"
		continue
	fi
	if [[ -z "$line" ]]; then
        echo "$line" >> "$todo_file"
		continue
    fi
	
	# 不是以 pick 开头的行， 原样输出
	if echo "$line" | grep -qv '^\s*pick'; then
        echo "$line" >> "$todo_file"
		continue
	fi
		
	commit=$(extract_cid "$line")
    msg=$(git log -1 --format=%s $commit)
    
    # 检查这个commit是否是一个merge commit
    if [ $(git rev-parse --verify $commit^2 2>/dev/null) ]; then
        echo "# skip $commit $msg (merge commit)" >> "$todo_file"
        continue
    fi
    
    # 检查这个commit的message是否已经在基础分支中存在
    if grep -qFx "$msg" "$base_msgs_file"; then
        echo "drop $commit $msg # 已在基础分支中存在" >> "$todo_file"
    else
        echo "pick $commit $msg" >> "$todo_file"
    fi
done

# 处理后的rebase数据复制
cp $todo_file $2

# 临时文件不再需要, 删除它们
rm -f "$todo_file" "$base_msgs_file"

# 启动编辑器
vi $2

#exit 1

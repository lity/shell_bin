#!/bin/bash

# 备份数据库目录
BAK_DIR=/data/database/mysql

# mysqldump options
DUMP_OPTIONS="--add-drop-database --add-drop-table"

# 备份一个数据库时失败时, 最大尝试次数
TRY_MAX_COUNT=3;

# 需要备份的数据库数组
#DUMP_DATABASE=(admin dspdemo fastadmin service tqb)
DUMP_DATABASE=(ad admin dspdemo fastadmin huitoutiao service tqb)

# 备份单一数据库
function dump_single_database() {
	let try_count=0;
	for (( i = 0; i < ${TRY_MAX_COUNT}; ++i )); do
	    mysqldump $1 $2 $3 > $4;
		if [[ $? -eq 0 ]]; then
			return 0;
		else
			let try_count++;
		fi
		if [[ $try_count -lt $TRY_MAX_COUNT ]]; then
			sleep 5;
			continue;
		fi
	done
	return 1;
};

bakSqlFile=""
success=0;
for (( VAR = 0; VAR < ${#DUMP_DATABASE[@]}; ++VAR )); do
	dump_db=${DUMP_DATABASE[$VAR]};
	dump_sql="${BAK_DIR}/${dump_db}.sql";
	bakSqlFile="${bakSqlFile} ${dump_db}.sql";
	dump_single_database "$*" "$DUMP_OPTIONS" "$dump_db" "$dump_sql";
	if [[ $? -ne 0 ]]; then
		success=1;
		break;
	fi
done
if [[ $success -ne 0 ]]; then
	echo 'bakup fail';
	exit 1;
fi

# gzip 文件名
gzipFileName=$(date "+%Y%m%d%H%M%S")
gzipFileName="${BAK_DIR}/${gzipFileName}.tar.gz"

# 创建压缩文件
tar -zcvf ${gzipFileName} -C ${BAK_DIR} ${bakSqlFile}
echo 'backup success';

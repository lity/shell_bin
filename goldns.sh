#!/bin/bash


cat domain.txt | while read file;
do
	file=${file#*//}; file=${file%%/*};
	ip=$(nslookup $file | grep "Address:" | grep -v "#")
	echo $ip;
done

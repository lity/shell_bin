var fs= require('fs');  
var data = fs.readFileSync('file','utf-8');  

var json  = JSON.parse(data);

var provinceList = findLevel(1, 0, json);
//console.log(JSON.stringify(provinceList));

var cityList = new Array();
for (i in provinceList) {
    var province = provinceList[i];
    var citys = findLevel(province.s_level + 1, province._id, json);
    province.nextLevel = citys;
    cityList = cityList.concat(citys);
}

for (i in cityList) {
    var city = cityList[i];
    citys = findLevel(city.s_level + 1, city._id, json);
    city.nextLevel = citys;
}


console.log(JSON.stringify(provinceList));
// console.log(data);

function findLevel(level, parentId, data) {
    var result = new Array();
    var i;
    for(i in  data) {
        var item = data[i];
        if (item.s_level == level && item.parentid == parentId) {
            result.push(item);
        }
    }
    return result;
}
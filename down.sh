#!/bin/sh
rm -fv result error ok
for i in $(seq 3442 1 10220)
do
	url="http://www.00394.net/plus/download.php?open=0&aid=$i&cid=3";
	curl -o data --connect-timeout 20 --max-time 30 "$url";
	data=`cat data | iconv -f gbk -t utf-8`
	ads=`echo $data | grep -oE 'href="[^"]*?\.mp3[^"]*?"' | sed 's/^href="//' | sed 's/"$//'`
	if [ "$ads" = "" ]; then
		echo "fail to parse $url";
		echo "fail to parse $url" >> error;
	else 
		echo "parse $url to downUrl $ads";
		echo "parse $url to downUrl $ads" >> ok;
		echo $ads >> result;
		
		#ads=`echo -n $ads | php -r "echo urldecode(fgets(STDIN));"`
		text=${ads%\?*}
		echo "text=${text}"
		pathPref=${text%/*}
		fileName=${text##*/}
		pathPost=${ads#*\?}

		if [[ "$fileName" =~ "%" ]]; then
			encodeFileName="$fileName"
			fileName=`echo -n "$fileName" | urldecodev`
		else
			encodeFileName=`echo -n "$fileName" | urlencodev`
		fi

		if [ ! -f "$fileName" ]; then
			adsEncode="$pathPref/$encodeFileName?$pathPost"
			echo "adsEncode=$adsEncode"
			curl -o "$fileName" \
				--referer "$url" \
				--user-agent "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36" \
				--speed-limit 1 --speed-time 30 \
				"$adsEncode"
			if [[ $? -ne 0 ]]; then
				rm "$fileName"
			fi
		fi
	fi
done

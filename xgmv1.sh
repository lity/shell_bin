#!/bin/bash
count=$(find . -type f | xargs wc -l)
find . -type f | 
while read file; 
do 
	ext=${file##*\.}; 
	newfile=$file"_tmp";
	if [ ext = "json" -o ext = "ExportJson" ]; then
		json-minify "$file" > "$newfile"
	fi
	
	mv "$newfile" "$file";
	echo "$file";
done
echo $count

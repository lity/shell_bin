#!/bin/bash
find . -type f | 
while read file; 
do 
	md5=$(md5sum "$file"); 
	ext=${file##*\.}; 
	name=${file%\.*};
	md5=${md5:0:6}; 
	newfile=$name"_"$md5.$ext;

	line=${name:2};
	line="\"$line"_"$md5.$ext\":$md5";
	mv "$file" "$newfile"; 
	echo $line;
done